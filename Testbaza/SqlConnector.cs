﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace SQLTestBaza
{
    public class SqlConnector
    {
        static private string _connectionString;

        public static void ConnectToCatalog(string Testbaza)
        {
            _connectionString = "Data Source=DESKTOP-ND6JVS4; " +
                $"Initial Catalog= {Testbaza}; Integrated Security=True; ";
        }
        static public DataTable Execute(string sqlRequest)
        {
            var dataSet = new DataSet();
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = sqlRequest;

                    var adapter = new SqlDataAdapter(sqlCommand);

                    adapter.Fill(dataSet);
                }
            }
            if (sqlRequest.StartsWith("SELECT") && dataSet.Tables[0].Rows[0] != null)
            {
                return dataSet.Tables[0];
            }
            else
            {
                throw new Exception("No write");
            }
        }
    }
}

