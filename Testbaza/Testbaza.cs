﻿using System;
using System.Collections;
using System.Data;
using NUnit.Framework;

namespace SQLTestBaza
{
    
    public class Testbaza
    {
        [TestCase("1", 0)]
       
        public void SelectOrderTable(string result, int index)
        {
            SqlConnector.ConnectToCatalog("Testbaza");
            DataTable responce = SqlConnector.Execute("SELECT PersonID FROM Orders Where ID = 1;");
            Assert.AreEqual(result, responce.Rows[index].ItemArray[0].ToString());
        }

        [TestCase("155")]
        
        public void SelectCount( string price)
        {
            SqlConnector.ConnectToCatalog("Testbaza");
            
            DataTable responce = SqlConnector.Execute($"SELECT Price FROM Orders Where ID = 2");
            
            Assert.AreEqual(price, responce.Rows[0].ItemArray[0].ToString());
        }

        [TestCase("38")]
        
        public void TableBase(string age)
        {
            SqlConnector.ConnectToCatalog("Testbaza");
            DataTable responce = SqlConnector.Execute($"SELECT Age FROM Person Where ID = 17 ;");
            Assert.AreEqual(age, responce.Rows[0].ItemArray[0].ToString());
        }
       
        [TestCase("1")]
        public void TableBaseMin(string result)
        {
            SqlConnector.ConnectToCatalog("Testbaza");
            DataTable responce = SqlConnector.Execute("SELECT MIN(ID)FROM Person;");
            Assert.AreEqual(result, responce.Rows[0].ItemArray[0].ToString());
        }

        [TestCase("Horokhiv")]
        
        public void SelectCityID(string result)
        {
            SqlConnector.ConnectToCatalog("Testbaza");
            DataTable responce = SqlConnector.Execute("SELECT City FROM Person WHERE ID=20;");
            Assert.AreEqual(result,responce.Rows[0].ItemArray[0].ToString());
        }
        [TestCase("Lena",10)]
       
        public void SelectInnerJoin(string result, int index)
        {
            SqlConnector.ConnectToCatalog("Testbaza");
            DataTable responce = SqlConnector.Execute("SELECT *FROM Person INNER JOIN Orders ON Person.ID = Orders.PersonID WHERE Person.ID=9");
            Assert.AreEqual(result, responce.Rows[index].ItemArray[0].ToString());
        }



    }
}
